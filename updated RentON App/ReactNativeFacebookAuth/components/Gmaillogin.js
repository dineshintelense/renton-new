import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet } from 'react-native'
import { createStackNavigator, createAppContainer } from "react-navigation";

import axios from "axios"
class Inputs extends Component {
    state = {
        email: '',
        password: ''
    }
    handleEmail = (text) => {
        this.setState({ email: text })
    }
    handlePassword = (text) => {
        this.setState({ password: text })
    }
    login1 = (email, pass) => {
        alert('email: ' + email + ' password: ' + pass)
    }

    login = (email, pass) => {
        let data = {
            username: email,
            password: pass
        }
        // alert('email: ' + data.number + ' password: ' + pass)
        axios.post("http://localhost:3000/login", data).then(resp => {

            if (resp.data === "Admin Login") {
                this.props.navigation.navigate('Productadd')

            } else {
                if (resp.data === "Invalid password") {
                    alert('Invalid password')
                } else {
                    this.props.navigation.navigate("Productscreen")
                }
            }
        })
            .catch(err => {
                console.log(err);
            })

    }
    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.input}
                    underlineColorAndroid="transparent"
                    placeholder="Email"
                    placeholderTextColor="#9a73ef"
                    autoCapitalize="none"
                    onChangeText={this.handleEmail} />

                <TextInput style={styles.input}
                    underlineColorAndroid="transparent"
                    placeholder="Password"
                    placeholderTextColor="#9a73ef"

                    autoCapitalize="none"
                    onChangeText={this.handlePassword} />


                <TouchableOpacity
                    style={styles.submitButton}
                    onPress={
                        () => this.login(this.state.email, this.state.password)
                    }>
                    <Text style={styles.submitButtonText}> Submit </Text>
                </TouchableOpacity>
            </View>
        )

    }



}
export default Inputs

const styles = StyleSheet.create({
    container: {
        paddingTop: 23
    },
    input: {
        margin: 15,
        height: 40,
        borderColor: '#7a42f4',
        borderWidth: 1
    },
    submitButton: {
        backgroundColor: '#7a42f4',
        padding: 10,
        margin: 15,
        height: 40,
    },
    submitButtonText: {
        color: 'white'
    }
})
