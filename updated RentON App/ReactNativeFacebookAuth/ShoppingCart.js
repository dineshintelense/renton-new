import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import Productscreen from "./containers/Productscreen";
import ProductDetailsScreen from "./containers/ProductDetails";
import ShoppingCartIcon from "./containers/ShoppingCartIcon";
import CartScreen from "./containers/CartScreen";
import buttonScreen from "./containers/button";
import { MaterialIcons } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import { EvilIcons } from "@expo/vector-icons";
import SearchIcon from "./containers/SearchIcon";
import HomeScreen1 from "./components/Homescreen";
import AboutScreen from "./components/Aboutscreen";
import Gmaillogin from "./components/Gmaillogin";
import OTP from "./components/OTP";
import Productadd from "./components/Productadd";
import Productremove from "./components/productremove";

import {
  HomeScreen,
  LoginScreen,
  RegisterScreen,
  ForgotPasswordScreen,
  Dashboard,
} from "./screens";

export default class ShoppingCart extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const AppNavigator = createStackNavigator(
  {
    
    Home1: {
      screen: HomeScreen1,
      navigationOptions: {
        header: null,
      }

    },
    
    HomeScreen,
    LoginScreen,
    RegisterScreen,
    ForgotPasswordScreen,
    Dashboard,



    Productadd: {
      screen: Productadd,
      navigationOptions: {
        header: null,
      }

    },

    Productremove: {
      screen: Productremove,
      navigationOptions: {
        header: null,
      }

    },
    Productscreen: {
      screen: Productscreen,
      navigationOptions: {
        header: null

      }
    },

    ProductDetails: {
      screen: ProductDetailsScreen,
      navigationOptions: {
        header: null
      }
    },

    Cart: {
      screen: CartScreen,
      navigationOptions: {
        headerTitle: "Discover",
        headerRight: <ShoppingCartIcon />
      }
    }
    ,


    Gmaillogin: {
      screen: Gmaillogin,
      navigationOptions: {
        header: null,
      }

    },
    OTP: {
      screen: OTP,
      navigationOptions: {
        header: null,
      }
    },
    About: {
      screen: AboutScreen,
      navigationOptions: {
        header: null,
      }
    },

  }

);
const AppContainer = createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
