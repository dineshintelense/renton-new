import React, { memo, useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import Background from "../components/Background";
import Logo from "../components/Logo";
import Header from "../components/Header";
import Button from "../components/Button";
import TextInput from "../components/TextInput";
import BackButton from "../components/BackButton";
import { theme } from "../core/theme";
import {
  emailValidator,
  passwordValidator,
  nameValidator,
} from "../core/utils";
import axios from "axios";
const RegisterScreen = ({ navigation }) => {
  const [name, setName] = useState({ value: "", error: "" });
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });
  const [inputVal1, setInputVal1] = useState("");
   const [inputVal2, setInputVal2] = useState("");  
  const [inputVal3, setInputVal3] = useState("");
  const _onSignUpPressed = () => {
    const nameError = nameValidator(name.value);
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError || nameError) {
      alert("Invalid Email")
      setName({ ...name, error: nameError });
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });}
      else{

      let data = {
        username: name.value,
        email: email.value,
        password: password.value

      }

      axios.post("http://localhost:3000/signup", data).then(resp => {
        alert(resp.data)
        if (resp.data === "Email already registered") {
          navigation.navigate('RegisterScreen')

        } else {
          if (resp.data === "Successfully registered") {
            navigation.navigate('LoginScreen')
          }
        }
      })
        .catch(err => {
          console.log(err);
        })








    }
  }
  

  return (
    <View style={styles.container}>
      <Background>
        <BackButton goBack={() => navigation.navigate("HomeScreen")} />

        <Logo />

        <Header>Create Account</Header>

        <TextInput
           value={inputVal1}
          onChange={(e) => setInputVal1(e.target.value)}
          style={styles.inputtext}
          label="Name"
          returnKeyType="next"
          value={name.value}
          onChangeText={(text) => setName({ value: text, error: "" })}
          error={!!name.error}
          errorText={name.error}
        />

        <TextInput
         value={inputVal2}
        onChange={(e) => setInputVal2(e.target.value)}
          style={styles.inputtext}
          label="Email"
          returnKeyType="next"
          value={email.value}
          onChangeText={(text) => setEmail({ value: text, error: "" })}
          error={!!email.error}
          errorText={email.error}
          autoCapitalize="none"
          autoCompleteType="email"
          textContentType="emailAddress"
          keyboardType="email-address"
        />

        <TextInput
         value={inputVal3}
        onChange={(e) => setInputVal3(e.target.value)}
          style={styles.inputtext}
          label="Password"
          returnKeyType="done"
          value={password.value}
          onChangeText={(text) => setPassword({ value: text, error: "" })}
          error={!!password.error}
          errorText={password.error}
          secureTextEntry
        />

        <Button
        disabled={!inputVal1 || !inputVal2 || !inputVal3}
          mode="contained"
          onPress={_onSignUpPressed}
          style={styles.button}
        >
          Sign Up
        </Button>

        <View style={styles.row}>
          <Text style={styles.label}>Already have an account? </Text>
          <TouchableOpacity onPress={() => navigation.navigate("LoginScreen")}>
            <Text style={styles.link}>Login</Text>
          </TouchableOpacity>
        </View>
      </Background>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,

    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    width: 360,
    height: 600,
  },
  label: {
    color: theme.colors.secondary,
  },
  button: {
    marginTop: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary,
  },
  inputtext: {
    borderRadius: 25,
    justifyContent: "center",
    marginBottom: -15,
  },
});

export default memo(RegisterScreen);
