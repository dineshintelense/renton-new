import React, { memo, useState, Component } from "react";
import { TouchableOpacity, StyleSheet, Text, View } from "react-native";
import Background from "../components/Background";
import Logo from "../components/Logo";
import Header from "../components/Header";
import Button from "../components/Button";
import TextInput from "../components/TextInput";
import BackButton from "../components/BackButton";
import { theme } from "../core/theme";
import { createStackNavigator, createAppContainer } from "react-navigation";
import axios from "axios"
import { emailValidator, passwordValidator } from "../core/utils";

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });

  const _onLoginPressed = () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError) {
      alert("please enter a valid email")
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError })
    }
    else {
      let data = {
        email: email.value,
        password: password.value
      }


      // alert('email: ' + data.number + ' password: ' + pass)
      axios.post("http://localhost:3000/login", data).then(resp => {
        alert(resp.data)
        if (resp.data === "Admin Login") {
          navigation.navigate('Productadd')

        } else {
          if (resp.data === "Invalid password") {
            alert('Invalid password')
          } else {
            navigation.navigate("Productscreen")
          }
        }
      })
        .catch(err => {
          console.log(err);
        })






    }
  }

  //navigation.navigate("Dashboard");


  return (
    <View style={styles.container}>
      <Background>
        <BackButton goBack={() => navigation.navigate("HomeScreen")} />

        <Logo />

        <Header>Welcome back.</Header>

        <TextInput
          style={styles.inputtext}
          label="Email"
          returnKeyType="next"
          value={email.value}
          onChangeText={(text) => setEmail({ value: text, error: "" })}
          error={!!email.error}
          errorText={email.error}
          autoCapitalize="none"
          autoCompleteType="email"
          textContentType="emailAddress"
          keyboardType="email-address"
        />

        <TextInput
          label="Password"
          returnKeyType="done"
          value={password.value}
          onChangeText={(text) => setPassword({ value: text, error: "" })}
          error={!!password.error}
          errorText={password.error}
          secureTextEntry
        />

        <View style={styles.forgotPassword}>
          <TouchableOpacity
            onPress={() => navigation.navigate("ForgotPasswordScreen")}
          >
            <Text style={styles.label}>Forgot your password?</Text>
          </TouchableOpacity>
        </View>

        <Button mode="contained" onPress={_onLoginPressed}>
          Login
        </Button>

        <View style={styles.row}>
          <Text style={styles.label}>Don’t have an account? </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate("RegisterScreen")}
          >
            <Text style={styles.link}>Sign up</Text>
          </TouchableOpacity>
        </View>
      </Background>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,

    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    width: 360,
    height: 600,
  },
  forgotPassword: {
    width: "100%",
    alignItems: "flex-end",
    marginBottom: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  label: {
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary,
  },
  inputtext: {
    borderRadius: 30,
    marginBottom: 1,
  },
});

export default memo(LoginScreen);
